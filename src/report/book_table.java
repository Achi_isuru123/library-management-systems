/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import DB.dbclass;
import java.awt.print.PrinterException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author isuru
 */
public class book_table extends javax.swing.JInternalFrame {

    public static Date d = new Date();
    public static SimpleDateFormat date1 = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat time1 = new SimpleDateFormat("HH:mm:ss");

    /**
     * Creates new form book_table
     */
    public book_table() {
        initComponents();
        this.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        BasicInternalFrameUI b1 = (BasicInternalFrameUI) this.getUI();
        b1.setNorthPane(null);
        bookcount();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        book_tables = new javax.swing.JTable();

        book_tables.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "BOOK ID", "USER NO", "AUTHOR", "BOOK NAME", "PUBLISH LOCATION", "PUBLISH DATE", "PAGE NO", "PRICE", "CATEGORY", "MEDIA", "TABLE NO", "ENTER DATE", "STATUS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, false, false, false, true, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(book_tables);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 739, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTable book_tables;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
 public static void LoadbookTable(String name) {
        try {
            DefaultTableModel dtm = (DefaultTableModel) book_tables.getModel();
            dtm.setRowCount(0);
            ResultSet rs = dbclass.search("select * from " + name);
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getString("bookid").toUpperCase());
                v.add(rs.getString("user_number"));
                v.add(rs.getString("author").toUpperCase());
                v.add(rs.getString("book_name").toUpperCase());
                v.add(rs.getString("public_loc").toUpperCase());
                v.add(rs.getString("publish_date"));
                v.add(rs.getString("number_page"));
                v.add(rs.getString("price"));
                v.add(rs.getString("categore_name"));
                v.add(rs.getString("media").toUpperCase());
                v.add(rs.getString("tableno").toUpperCase());
                v.add(rs.getString("Enter_time"));
                boolean string = rs.getBoolean("activestatus");
                if (string) {
                    String ac = "Active Book";
                    v.add(ac);
                }else{
                     String ina = "Inactive Book";
                    v.add(ina); 
                }
                dtm.addRow(v);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void date_select(String From_Date, String To_Date) {
        try {
            From_Date += " 00:00:00";
            To_Date += " 00:00:00";
            DefaultTableModel dtm = (DefaultTableModel) book_tables.getModel();
            dtm.setRowCount(0);
            ResultSet rs = dbclass.search("select * from add_book where Enter_time between '" + From_Date + "' and '" + To_Date + "'");
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getString("bookid").toUpperCase());
                v.add(rs.getString("user_number"));
                v.add(rs.getString("author").toUpperCase());
                v.add(rs.getString("book_name").toUpperCase());
                v.add(rs.getString("public_loc").toUpperCase());
                v.add(rs.getString("publish_date"));
                v.add(rs.getString("number_page"));
                v.add(rs.getString("price"));
                v.add(rs.getString("categore_name"));
                v.add(rs.getString("media").toUpperCase());
                v.add(rs.getString("tableno").toUpperCase());
                v.add(rs.getString("Enter_time"));
                boolean string = rs.getBoolean("activestatus");
                if (string) {
                    String ac = "Active Book";
                    v.add(ac);
                }else{
                     String ina = "Inactive Book";
                    v.add(ina); 
                }
                dtm.addRow(v);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void Dally_Report(String date1, String date2) {
        try {
            DefaultTableModel dtm = (DefaultTableModel) book_tables.getModel();
            dtm.setRowCount(0);
            date1 += " 00:00:00";
            date2 += " 23:59:59";

            ResultSet rs = dbclass.search("select * from add_book where Enter_time between '" + date1 + "' and '" + date2 + "'");
            System.out.println(date1);
            System.out.println(date2);
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getString("bookid").toUpperCase());
                v.add(rs.getString("user_number"));
                v.add(rs.getString("author").toUpperCase());
                v.add(rs.getString("book_name").toUpperCase());
                v.add(rs.getString("public_loc").toUpperCase());
                v.add(rs.getString("publish_date"));
                v.add(rs.getString("number_page"));
                v.add(rs.getString("price"));
                v.add(rs.getString("categore_name"));
                v.add(rs.getString("media").toUpperCase());
                v.add(rs.getString("tableno").toUpperCase());
                v.add(rs.getString("Enter_time"));
                boolean string = rs.getBoolean("activestatus");
                if (string) {
                    String ac = "Active Book";
                    v.add(ac);
                }else{
                     String ina = "Inactive Book";
                    v.add(ina); 
                }
                dtm.addRow(v);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void Active() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) book_tables.getModel();
            dtm.setRowCount(0);
            ResultSet rs = dbclass.search("select * from add_book");
            while (rs.next()) {
                boolean status = rs.getBoolean("activestatus");
                if (status) {
                    Vector v = new Vector();
                    v.add(rs.getString("bookid").toUpperCase());
                    v.add(rs.getString("user_number"));
                    v.add(rs.getString("author").toUpperCase());
                    v.add(rs.getString("book_name").toUpperCase());
                    v.add(rs.getString("public_loc").toUpperCase());
                    v.add(rs.getString("publish_date"));
                    v.add(rs.getString("number_page"));
                    v.add(rs.getString("price"));
                    v.add(rs.getString("categore_name"));
                    v.add(rs.getString("media").toUpperCase());
                    v.add(rs.getString("tableno").toUpperCase());
                    v.add(rs.getString("Enter_time"));
                    boolean string = rs.getBoolean("activestatus");
                if (string) {
                    String ac = "Active Book";
                    v.add(ac);
                }else{
                     String ina = "Inactive Book";
                    v.add(ina); 
                }
                    dtm.addRow(v);
                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Inactive() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) book_tables.getModel();
            dtm.setRowCount(0);
            ResultSet rs = dbclass.search("select * from add_book");
            while (rs.next()) {
                boolean status = rs.getBoolean("activestatus");
                if (status) {

                } else {
                    Vector v = new Vector();
                    v.add(rs.getString("bookid").toUpperCase());
                    v.add(rs.getString("user_number"));
                    v.add(rs.getString("author").toUpperCase());
                    v.add(rs.getString("book_name").toUpperCase());
                    v.add(rs.getString("public_loc").toUpperCase());
                    v.add(rs.getString("publish_date"));
                    v.add(rs.getString("number_page"));
                    v.add(rs.getString("price"));
                    v.add(rs.getString("categore_name"));
                    v.add(rs.getString("media").toUpperCase());
                    v.add(rs.getString("tableno").toUpperCase());
                    v.add(rs.getString("Enter_time"));
                    boolean string = rs.getBoolean("activestatus");
                if (string) {
                    String ac = "Active Book";
                    v.add(ac);
                }else{
                     String ina = "Inactive Book";
                    v.add(ina); 
                }
                    dtm.addRow(v);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Table() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) book_tables.getModel();
            dtm.setRowCount(0);
            ResultSet rs = dbclass.search("select * from add_book");
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getString("bookid").toUpperCase());
                v.add(rs.getString("user_number"));
                v.add(rs.getString("author").toUpperCase());
                v.add(rs.getString("book_name").toUpperCase());
                v.add(rs.getString("public_loc").toUpperCase());
                v.add(rs.getString("publish_date"));
                v.add(rs.getString("number_page"));
                v.add(rs.getString("price"));
                v.add(rs.getString("categore_name"));
                v.add(rs.getString("media").toUpperCase());
                v.add(rs.getString("tableno").toUpperCase());
                v.add(rs.getString("Enter_time"));
                boolean string = rs.getBoolean("activestatus");
                if (string) {
                    String ac = "Active Book";
                    v.add(ac);
                }else{
                     String ina = "Inactive Book";
                    v.add(ina); 
                }
                dtm.addRow(v);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ExportBookReport() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) book_tables.getModel();
            JFileChooser jFileChooser = new JFileChooser();
            int showOpenDialog = jFileChooser.showOpenDialog(null);
            File selectedFile = jFileChooser.getSelectedFile();
            FileWriter fileWriter = new FileWriter(selectedFile + ".(xlsx)");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (int i = 0; i < dtm.getColumnCount(); i++) {
                bufferedWriter.write(dtm.getColumnName(i) + "\t");
            }
            bufferedWriter.write("\n");
            for (int i = 0; i < dtm.getRowCount(); i++) {
                for (int j = 0; j < dtm.getColumnCount(); j++) {
                    bufferedWriter.write(dtm.getValueAt(i, j) + "\t");
                }
                bufferedWriter.write("\n");
            }
            bufferedWriter.close();
            JOptionPane.showMessageDialog(null, "Export Successfull");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bookcount() {
        try {
            ResultSet rs = dbclass.search("select count(*) as id_book from add_book where activestatus = 1");
            if (rs.next()) {
                int Count = rs.getInt("id_book");
                String counts = "" + Count;
                report.datareport.systembook.setText(counts);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printtable() {
        MessageFormat header = new MessageFormat("ALL BOOK");
        MessageFormat footer = new MessageFormat("(A / BUDDHANGALA CENTRAL COLLEAGE)");

        try {
            book_tables.print(JTable.PrintMode.FIT_WIDTH, header, footer);

        } catch (PrinterException e) {
            e.printStackTrace();
        }
    }
}
