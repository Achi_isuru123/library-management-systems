/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

/**
 *
 * @author isuru
 */
public class SysteConfigData {
    
    private static String activeuser;
    private static String usertype;

    public static String getActiveuser() {
        return activeuser;
    }

    public static void setActiveuser(String activeuser) {
        SysteConfigData.activeuser = activeuser;
    }

    public static String getUsertype() {
        return usertype;
    }

    public static void setUsertype(String usertype) {
        SysteConfigData.usertype = usertype;
    }
    
}
